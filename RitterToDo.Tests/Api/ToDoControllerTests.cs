﻿using FakeItEasy;
using Moo;
using NUnit.Framework;
using Ploeh.AutoFixture;
using RitterToDo.Api;
using RitterToDo.Core;
using RitterToDo.Models;
using RitterToDo.Repos;
using Should;

namespace RitterToDo.Tests.Controllers.Api
{
    [TestFixture]
    public class ToDoControllerTests
    {
        /// <summary>
        /// Subject Under Test
        /// </summary>
        /// <returns></returns>
        private ToDoController CreateSUT()
        {
            return new ToDoController(
                A.Fake<IRepository<ToDo>>(),
                A.Fake<ILookupHelper<ToDoCategory, ToDoCategoryViewModel>>(),
                A.Fake<IMappingRepository>());
        }

        [Test]
        public void GetApi_DefaultCase_ShowsToDoList()
        {
            var sut = CreateSUT();
            var fixture = new Fixture();
            var entities = fixture.CreateMany<ToDo>();
            var models = fixture.CreateMany<ToDoViewModel>();
            A.CallTo(() => sut.ToDoRepo.GetAll()).Returns(entities);
            var mapperMock = A.Fake<IExtensibleMapper<ToDo, ToDoViewModel>>();
            A.CallTo(() => sut.MappingRepository.ResolveMapper<ToDo, ToDoViewModel>()).Returns(mapperMock);
            A.CallTo(() => mapperMock.MapMultiple(entities)).Returns(models);

            var result = sut.Get();
            result.ShouldEqual(models);
        }

        [Test]
        public void GetById_Api_DefaultCase_ShowsToDoList()
        {
            var sut = CreateSUT();
            var fixture = new Fixture();
            var entities = fixture.CreateMany<ToDo>();
            var models = fixture.CreateMany<ToDoViewModel>();
            A.CallTo(() => sut.ToDoRepo.GetAll()).Returns(entities);
            var mapperMock = A.Fake<IExtensibleMapper<ToDo, ToDoViewModel>>();
            A.CallTo(() => sut.MappingRepository.ResolveMapper<ToDo, ToDoViewModel>()).Returns(mapperMock);
            A.CallTo(() => mapperMock.MapMultiple(entities)).Returns(models);

            var result = sut.Get();
            result.ShouldEqual(models);
        }

    }
}
