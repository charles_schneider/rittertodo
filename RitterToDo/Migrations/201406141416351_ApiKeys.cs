namespace RitterToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApiKeys : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserApiKeys",
                c => new
                    {
                        ApiKey = c.String(nullable: false, maxLength: 100),
                        UserId = c.String(nullable: false, maxLength: 100),
                        AppName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ApiKey);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserApiKeys");
        }
    }
}
