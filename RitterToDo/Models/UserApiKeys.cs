﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RitterToDo.Models
{
    public class UserApiKeys
    {
        [Key]
        [MaxLength(100)]
        public string ApiKey { get; set; }

        [Required]
        [MaxLength(100)]
        public string UserId { get; set; }

        [Required]
        [MaxLength(100)]
        public string AppName { get; set; }
    }
}