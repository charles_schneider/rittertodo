[assembly: WebActivator.PostApplicationStartMethod(typeof(RitterToDo.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace RitterToDo.App_Start
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Moo;
    using RitterToDo.Core;
    using RitterToDo.Models;
    using RitterToDo.Repos;
    using SimpleInjector;
    using SimpleInjector.Extensions;
    using SimpleInjector.Integration.Web.Mvc;
    using SimpleInjector.Integration.WebApi;
    using System.Reflection;
    using System.Threading;
    using System.Web.Http;
    using System.Web.Mvc;

    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC5 Dependency Resolver.</summary>
        public static void Initialize()
        {
            // Did you know the container can diagnose your configuration? Go to: https://bit.ly/YE8OJj.
            var container = new Container();

            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver =
               new SimpleInjectorWebApiDependencyResolver(container);
        }

        public static void InitializeContainer(Container container)
        {
            container.Register(
                () => (new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()))));

            container.RegisterPerWebRequest(
                () => Thread.CurrentPrincipal);

            container.Register<IIdentityHelper, IdentityHelper>();

            container.Register<IRepository<ToDo>, ToDoRepository>();

            container.RegisterOpenGeneric(typeof(IRepository<>), typeof(BaseRepository<>));

            container.RegisterOpenGeneric(typeof(ILookupHelper<,>), typeof(LookupHelper<,>));

            container.Register<IApplicationDbContext, ApplicationDbContext>();

            container.RegisterSingle(MappingRepository.Default);
        }
    }

}